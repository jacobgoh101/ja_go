
# JA_GO

A project.

# Live Application

**[http://ja_go.surge.sh/](http://ja_go.surge.sh/)**


## Built With

* [Handlebars.js](https://github.com/wycats/handlebars.js/) - HTML Templating Engine
* [Sass](https://sass-lang.com/) - CSS Preprocessor
* [PostCSS](https://github.com/postcss/postcss) - CSS Postprocessor (including Autoprefixer, CSSNano)
* [Gulp](https://gulpjs.com/) - Automating Handlebars.js, Sass and compilation
* [Webpack](https://github.com/webpack/webpack) - Javascript Module Bundler. Including [Babel](https://github.com/babel/babel) for transpiling ES6 .

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

 - NodeJS 8.*

### Installing

Install the project dependencies.

```
yarn
```
or
```
npm i
```

Install `gulp` globally.

```
npm i -g gulp
```

### Development

Start a development server. Watch and compile Handlebars.js and Sass.

```
npm run dev
```
Watch and compile Javascript. (*Entry: `src/js/index.js`*)

```
npm run webpack
```


## Author

* **[Jacob Goh](https://github.com/jacobgoh101/)**