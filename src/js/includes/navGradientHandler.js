document.querySelector('.profile-header__nav').addEventListener('scroll', (e) => {
    const gradientRight = document.querySelector('.profile-header__nav-gradient-right');
    if (e.currentTarget.scrollLeft > 10)
        gradientRight.classList.add('hidden');
    else
        gradientRight.classList.remove('hidden');
});