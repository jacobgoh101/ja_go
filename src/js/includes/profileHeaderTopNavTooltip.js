const logoutTooltipClass = "profile-header__top-nav__logout-tooltip";
const logoutTooltipClassActiveModifier = "--active";
const logoutLink = document.querySelector(".profile-header__top-nav__link--logout");
const logoutTooltip = document.querySelector(`.${logoutTooltipClass}`);

logoutLink.addEventListener('click', (e) => {
    logoutTooltip.classList.toggle(logoutTooltipClass + logoutTooltipClassActiveModifier);
});

// detect click outside elem
document.addEventListener('click', (e) => {
    const isClickInsideLogoutTooltip = logoutLink.contains(event.target) || logoutTooltip.contains(event.target);

    if (!isClickInsideLogoutTooltip) {
        logoutTooltip.classList.remove(logoutTooltipClass + logoutTooltipClassActiveModifier);
    }
})