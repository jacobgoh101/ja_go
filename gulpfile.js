'use strict';

const browserSync = require('browser-sync').create();
const del = require('del');
const env = require('gulp-util').env;
const gulp = require('gulp');
const handlebars = require('gulp-compile-handlebars');
const rename = require('gulp-rename');
const postcss = require('gulp-postcss');
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const sourcemaps = require('gulp-sourcemaps');

const config = {
    src: './src',
    dest: './dist',
    watchers: [{
        match: ['./src/templates/**/*.hbs'],
        tasks: ['html']
    }, {
        match: ['./src/scss/**/*.scss'],
        tasks: ['css']
    }]
};

/*
START HTML
 */
gulp.task('clean-html', () => del(`${config.dest}/*.html`));

gulp.task('html', ['clean-html'], () => {
    return gulp
        .src(`${config.src}/templates/pages/*.hbs`)
        .pipe(handlebars({}, {
            ignorePartials: true,
            batch: [`${config.src}/templates/partials`],
            helpers: {
                ifEquals: function(arg1, arg2, options) {
                    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
                }
            }
        }))
        .pipe(rename({
            extname: '.html'
        }))
        .pipe(gulp.dest(config.dest));
});
/*
END HTML
 */

/*
START CSS
 */
const scssPath = `${config.src}/scss/*.scss`;

gulp.task('css', function() {
    const processors = [
        autoprefixer({
            browsers: "last 3 versions"
        }),
        cssnano //minifies css
    ];
    return gulp
        .src(scssPath)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(sourcemaps.write('./sourcemaps'))
        .pipe(gulp.dest(config.dest));
});
/*
END CSS
 */

/*
START Handling assets
 */
gulp.task('clean-assets', () => del(`${config.dest}/assets`));

const sourceAssets = `${config.src}/assets/**/*`;

gulp.task('assets', ['clean-assets'], function() {
    return gulp
        .src([sourceAssets])
        .pipe(gulp.dest(config.dest + '/assets/'));
});
/*
END Handling assets
 */

gulp.task('serve', () => {
    browserSync.init({
        open: false,
        notify: false,
        files: [`${config.dest}/**/*`],
        server: config.dest
    });
});

gulp.task('watch', () => {
    config
        .watchers
        .forEach(item => {
            gulp.watch(item.match, item.tasks);
        });
});

gulp.task('default', ['html'], done => {
    if (env.dev) {
        gulp.start('serve');
        gulp.start('watch');
    }
    done();
});